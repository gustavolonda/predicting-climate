#!/usr/bin/python3
# -*- coding: utf-8 -*-

from logic import connectDB

class Forecasts(object):


    def __init__(self):

        # Atributos de la clase, son los campos de la tabla
        self.__id = 0
        self.__name_city = ''
        self.__forecast_day = 0
        self.__forecast_night = 0
        self.__creation_time = ''
        self.__modification_time = ''

   
    def __str__(self):
        return self.getName_city()

    # ------------------------- Getters y Setters -------------------------
	
    def getId(self):
        return self.__id
    def setId(self, valor):
        self.__id = valor

    def getName_city(self):
        return self.__name_city
    def setName_city(self, valor):
        self.__name_city = valor

    def getForecast_day(self):
        return self.__forecast_day
    def setForecast_day(self, valor):
        self.__forecast_day = valor

    def getForecast_night(self):
        return self.__forecast_night
    def setForecast_night(self, valor):
        self.__forecast_night = valor

    def getCreation_time(self):
        return self.__creation_time
    def setCreation_time(self, valor):
        self.__creation_time = valor

    def getModification_time(self):
        return self.__modification_time
    def setModification_time(self, valor):
        self.__modification_time = valor
# ----------------------- M�todos de Acceso a Datos para la clase -----------------------


def obtenerTodosList(self):
    lista = []
 
    bd = connectDB.ConectDB()
    if (bd.get_db() == None):
        lista = None
        return lista
    else:
        cursor = bd.get_db().cursor()
        cursor.execute('select * from forecast_t')       #filas = cursor.fetchall()
        #cursor.close()
        for columna in cursor:
            lista.append(columna)
        cursor.close()
        return lista

# ---------------------------------------------------------------------------------------
def searchTodosList(self,fecha):
    lista = []

    bd = connectDB.ConectDB()
    sql = "select * from forecast_t where creation_time like '{0}%'".format(fecha)
    if (bd.get_db() == None):
        lista = None
        return lista
    else:
        cursor = bd.get_db().cursor()
        cursor.execute(sql)      
        for columna in cursor:
            lista.append(columna)
        cursor.close()
        return lista

def edit_forecast(self, name_city, forecast_day,forecast_night,id):
    salida = False
    bd =  connectDB.ConectDB()
    if (bd.get_db() == None):
        return salida
    else:
        cursor =  bd.get_db().cursor()
        sql="UPDATE forecast_t SET name_city = %s, forecast_day = %s, forecast_night = %s   WHERE id = %s"
        val = (name_city, int(forecast_day), int(forecast_night),int(id))
        cursor.execute(sql, val)
        print(id)
        bd.get_db().commit()
        cursor.close()
        salida = True
        return salida
def new(forecast):
   
    salida = False
    bd =  connectDB.ConectDB()
    if (bd.get_db() == None):
        return salida
    else:
        cursor = bd.get_db().cursor()
        sql = "INSERT INTO forecast_t (name_city, forecast_day, forecast_night) VALUES (%s, %s, %s)"
        val = (forecast.getName_city(), int(forecast.getForecast_day()), int(forecast.getForecast_night()))
        cursor.execute(sql, val)
        bd.get_db().commit()
        cursor.close()
        salida = True
        return salida

# ---------------------------------------------------------------------------------------


