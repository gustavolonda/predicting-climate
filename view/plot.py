#!/usr/bin/python3

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gdk, Gtk
from os.path import abspath, dirname, join

from matplotlib.figure import Figure
from numpy import arange, pi, random, linspace
import matplotlib.cm as cm
from matplotlib.collections import EventCollection
import numpy as np
#Possibly this rendering backend is broken currently
#from matplotlib.backends.backend_gtk3agg import FigureCanvasGTK3Agg as FigureCanvas
from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
from matplotlib.ticker import MaxNLocator

class Plot(object):
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_objects_from_file('view/plot.glade', ('window1', '') )
        builder.connect_signals(Signals())
        self.myfirstwindow = builder.get_object('window1')
        self.sw = builder.get_object('scrolledwindow1')
        print("heightjbjghg")
    def createPlot(self,listaSearch,fecha):
        np.random.seed(19680801)
        fig = Figure()
        ax = fig.add_subplot(1, 1, 1)
        prng = np.random.RandomState(96917002)
        plot_bar_graphs(ax, listaSearch)
        n_groups = 5
        index = np.arange(n_groups)
        bar_width = 0.35
        ax.set_xlabel("City's names   "+fecha)
        ax.set_ylabel('Degrees Celsius')
        ax.set_title('Weather forecast chart')
        ax.set_xticks(index + bar_width / 2)
        self.listsNameCity=[]
        for i in range(len(listaSearch)):
            self.listsNameCity.append(listaSearch[i][1])
        ax.set_xticklabels(self.listsNameCity)
        #fig.tight_layout()
        ax.plot()
        canvas = FigureCanvas(fig)
        self.sw.add_with_viewport(canvas)
        self.myfirstwindow.show_all()
        Gtk.main()
    


class Signals:
    def on_window1_destroy(self, widget):
        Gtk.main_quit()



def ghgsadiaso(ax,rects, xpos='center'):
	xpos = xpos.lower() 
	ha = {'center': 'center', 'right': 'left', 'left': 'right'}
	offset = {'center': 0.5, 'right': 0.57, 'left': 0.43} 
	for rect in rects:
		height = rect.get_height()
		ax.text(rect.get_x() + rect.get_width()*offset[xpos], 1.01*height,'{}'.format(height), ha=ha[xpos], va='bottom')
	return ax

def plot_bar_graphs(ax, listaSearch, min_value=5, max_value=25):
    nb_samples=len(listaSearch)
    opacity = 0.4
    error_config = {'ecolor': '0.3'}
    x = np.arange(nb_samples)
    ya=[]
    yb=[]
    for i in range(len(listaSearch)):
            ya.append(listaSearch[i][2])
    for i in range(len(listaSearch)):
            yb.append(listaSearch[i][3])
    width = 0.25
    std_women = (3, 5, 2, 3, 3)
    rects1=ax.bar(x, ya, width, alpha=opacity, color='b')
    rects2=ax.bar(x + width, yb, width,alpha=opacity,  color='C2')
    ghgsadiaso(ax,rects1, "left")
    ghgsadiaso(ax,rects2, "right")
    area2 = 0.2 * 0.3
    ax.legend((rects1[0], rects2[0]), ('Day', 'Night'))
    ax.set_xticks(x + width)
    return ax


