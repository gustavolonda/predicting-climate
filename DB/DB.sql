CREATE DATABASE weatherDB;
CREATE TABLE `weatherDB`.forecast_t (
    id int NOT NULL AUTO_INCREMENT,
    name_city varchar(255) NOT NULL,
    forecast_day int,
    forecast_night int,
    creation_time     DATETIME DEFAULT CURRENT_TIMESTAMP,
    modification_time DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);
