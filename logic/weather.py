#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Archivo: weather.py Gustavo I. Londa M. 02/11/2018 
Description: Load the weather forecast, get it from weather.com 
"""
from logic import pywapi
import string
import datetime

def get_weather_from_weather_com(name_city):
	#locate =string.lower(name_city)
	locate =name_city.lower()
	locatMap = pywapi.get_location_ids(locate)
	keylist = []
	for key in locatMap.keys():
		keylist.append(key) 
	#usekey =  keylist[0].decode('utf-8')
	usekey =  keylist[0]
	print(usekey)
	today= datetime.datetime.today().weekday()
	weather_com_result = pywapi.get_weather_from_weather_com(usekey)
	return weather_com_result
