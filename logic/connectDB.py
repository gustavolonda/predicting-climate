#!/usr/bin/python3

# -*- coding: utf-8 -*-
"""
Archivo: connectDB.py Gustavo I. Londa M. 02/11/2018 
Description:Conection to Date Base
"""
#import MySQLdb
#!/usr/bin/python3

import pymysql
class ConectDB(object):

    def __init__(self):
    	self.__db = None
    	try:
            
            self.__db = pymysql.connect(host="localhost",    # your host, usually localhost
                     user="weather",         # your username
                     passwd="root",  # your password
                     db="weatherDB")        # name of the data base
    	except:
        	self.__db = None
    def get_db(self):
        return self.__db



if __name__ == '__main__':
    a = ConectDB()
    if (a.get_db() == None):
        print ('Sin conexion')
    else:
        print ('OK')
