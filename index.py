#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Archivo: index.py Gustavo I. Londa M. 02/11/2018 
Description: load index.glade 
"""
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gdk, Gtk
from os.path import abspath, dirname, join
from logic import weather
from model import forecasts
from view import plot
import datetime
import string
WHERE_AM_I = abspath(dirname(__file__))
class Index(object):

	def __init__(self):
		objsW = Gtk.Builder()
		objsW.add_from_file('view/index.glade')
		self.winMain = objsW.get_object('winMain')
		hendlers={
		"terminar_aplicacion":Gtk.main_quit}
		objsW.connect_signals(self)
		self.winMain.show_all()
		button_search = objsW.get_object("button_search")
		self.entry_city = objsW.get_object("entry_city")
		self.result_prediction = objsW.get_object("result_prediction")
		self.treeView = objsW.get_object("treeView")
		self.liststore = objsW.get_object("liststore1")
		self.down_dialog = objsW.get_object("down_dialog")
		self.message_dialog=objsW.get_object("message_dialog")
		self.message_date_dialog = objsW.get_object("message_date_dialog")
		self.message_edited = objsW.get_object("message_edited")
		self.message_edited_error = objsW.get_object("message_edited_error")
		self.message_no_select_dato = objsW.get_object("message_no_select_dato")
		self.message_no_entry_name = objsW.get_object("message_no_entry_name")
		self.entry_edit_name = objsW.get_object("entry_edit_name")
		self.entry_edit_day = objsW.get_object("entry_edit_day")
		self.entry_edit_night = objsW.get_object("entry_edit_night")
		self.entry_edit_date = objsW.get_object("entry_edit_date")

		self.lista = forecasts.obtenerTodosList(self)
		self.show_list(self.lista)
		self.calendar = Gtk.Calendar ()
		self.cwindow = Gtk.Window(Gtk.WindowType.TOPLEVEL)
		self.display = False
		self.currentDate = datetime.date.today()
		#self.cwindow.set_position (Gtk.WIN_POS_MOUSE)
		self.cwindow.set_decorated (False)
		self.cwindow.set_modal (True)
		self.al_vbox = Gtk.VBox(False, 10)
		self.buttonOK=Gtk.Button("OK")
		self.buttonOK.connect('clicked', self.calendar_hide, self.cwindow)
		self.al_vbox.pack_start(self.calendar, True, False, 0)
		self.al_vbox.pack_start(self.buttonOK, True, False, 0)
		self.cwindow.add (self.al_vbox)
		self.calendar_find_date = objsW.get_object("calendar_find_date")
		self.entry_find_date = objsW.get_object("entry_find_date")
		self.entry_find_date.set_width_chars (10)
		self.entry_find_date.connect("activate", self.calendar_show, self.cwindow)
		self.winMain.connect("configure-event",self.on_window_config,self.calendar_find_date,self.cwindow)
		self.connect_signals ()
		self.update_entry ()
		#self.set_style()
		self.listaSearch = []
		


	def on_winMain_destroy(self, window):
		Gtk.main_quit()



	

	def show_result(self,button):
		if self.entry_city.get_text ()=="":
			self.show_message_no_entry_name()
		else:
			self.show_down_dialog()
				
			try:
				self.result_prediction.set_text("")
				weather_com_result = weather.get_weather_from_weather_com(self.entry_city.get_text())
				diaT = int(  weather_com_result['forecasts'][1]["high"] ) 
				nocheT = int(  weather_com_result['forecasts'][1]['low'] )
				self.result_prediction.set_text("The forecast for tomorrow's weather will be "+str(diaT)+"C day and "+str(nocheT)+"C night")
				forecast = forecasts.Forecasts()
				forecast.setName_city(self.entry_city.get_text().upper())
				forecast.setForecast_day(diaT)
				forecast.setForecast_night(nocheT)
				if forecasts.new(forecast):
					self.lista = forecasts.obtenerTodosList(self)
					self.show_list(self.lista)

				else: 
					print("no Guardado")
			except:	
				self.hide_down_dialog()
				self.show_message_dialog()
			self.hide_down_dialog()
	def click_find_button_clear(self,button):
		self.result_prediction.set_text("")
		self.entry_city.set_text("")

		
	def on_Button_Edit(self, b, b2, w):
		# Nos servimos de tree iterator para recuperar los datos de la fila seleccionada
		select = self.treeView.get_selection()
		model, treeiter = select.get_selected()
		self.name_city = self.liststore[treeiter][0]
		self.forecast_day = int(self.liststore[treeiter][1])
		self.forecast_night = int(self.liststore[treeiter][2])
		edit_date = self.liststore[treeiter][3]
		self.idtreeview=int(self.liststore[treeiter][4])
		# Preparamos los contenedores
		self.entry_edit_name.set_text(self.name_city)
		self.entry_edit_day.set_text(str(self.forecast_day))
		self.entry_edit_night.set_text(str(self.forecast_night ))
		self.entry_edit_date.set_text(edit_date)
	def click_edit_button_edit(self,button):
		if self.entry_edit_date.get_text()=="":
			self.show_message_no_select_dato()
		else:
			forecast = forecasts.Forecasts()
			result=forecasts.edit_forecast( self,self.entry_edit_name.get_text(), self.entry_edit_day.get_text(),self.entry_edit_night.get_text(),self.idtreeview)
			if result==True:
				self.show_message_dialog_edited()
				self.lista = forecasts.obtenerTodosList(self)
				self.show_list(self.lista)
				self.entry_edit_name.set_text("")
				self.entry_edit_day.set_text("")
				self.entry_edit_night.set_text("")
				self.entry_edit_date.set_text("")
			else:
				self.show_message_dialog_edited_error()
	def click_edit_button_cancel(self,button):
		self.lista = forecasts.obtenerTodosList(self)
		self.show_list(self.lista)
		self.entry_edit_name.set_text("")
		self.entry_edit_day.set_text("")
		self.entry_edit_night.set_text("")
		self.entry_edit_date.set_text("")
	

	def show_list(self, lista):
		self.liststore.clear()
		for i in range(len(lista)):
			self.liststore.append([lista[i][1], lista[i][2], lista[i][3], '{:%Y-%m-%d}'.format(lista[i][4]), int(lista[i][0])])
	def calendar_show(self, button):
		self.cwindow.show_all ()
	def connect_signals (self):
		self.day_selected_handle = self.calendar.connect ('day-selected', self.update_entry)
		self.day_selected_double_handle = self.calendar.connect ('day-selected-double-click', self.calendar_hide)
		self.activate = self.entry_find_date.connect ('activate', self.update_calendar)
		#self.focus_out = self.entry.connect ('focus-out-event', self.focus_out_event)
	def calendar_hide(self, button, *args):
		self.cwindow.hide ()
	def apply_screen_coord_correction(self, x, y, widget, relative_widget):
		corrected_y = y
		corrected_x = x
		rect = widget.get_allocation()
		screen_w = 400
		screen_h = 500
		delta_x = screen_w - (x + rect.width)
		delta_y = screen_h - (y + rect.height)
		if delta_x < 0:
			corrected_x += delta_x
		if corrected_x < 0:
			corrected_x = 0
		if delta_y < 0:
			corrected_y = y - rect.height - relative_widget.get_allocation().height
		if corrected_y < 0:
			corrected_y = 0
		return [corrected_x, corrected_y]
	def on_window_config(self, widget, event, toggle_button, cal_window):
		# Maybe better way to find the visiblilty
		if cal_window.get_mapped():
			rect = toggle_button.get_allocation()
			cal_x = event.x + rect.x
			cal_y = event.y + rect.y + rect.height
			[x, y] = self.apply_screen_coord_correction(cal_x, cal_y, cal_window, toggle_button)
			cal_window.move(x, y)
	def update_calendar (self, *args):
		try:
			dt = datetime.datetime.strptime (self.entry_find_date.get_text (), "%Y-%m-%d")
		except:
			try:
				dt = datetime.datetime.strptime (self.entry_find_date.get_text (), "%Y-%m-%d")
			except:
				logging.info ('CalendarEntry.update_calendar: Error parsing date, setting it as today...')
				dt = datetime.date.fromtimestamp(time.time())
				self.set_date (dt)
	def update_entry (self, *args):
		year,month,day = self.calendar.get_date ()
		month = month +1;
		self.currentDate = datetime.date(year, month, day)
		text = self.currentDate.strftime ("%Y-%m-%d")
		self.entry_find_date.set_text (text)
	def set_date (self, date):
		if not date:
			date = datetime.date.fromtimestamp (time.time())
			self.currentDate = date
			self.__block_signals ()
			self.calendar.select_day (1)
			self.calendar.select_month (self.currentDate.month-1, self.currentDate.year)
			self.calendar.select_day (self.currentDate.day)
			self.__unblock_signals ()
			self.update_entry ()

	def get_date (self):
		return self.currentDate

	def __block_signals (self):
		self.calendar.handler_block (self.day_selected_handle)
		self.calendar.handler_block (self.day_selected_double_handle)
		self.calendar_find_date.handler_block (self.clicked_handle)
		self.entry_find_date.handler_block (self.activate)
		self.entry_find_date.handler_block (self.focus_out)
	def __unblock_signals (self):
		self.calendar.handler_unblock (self.day_selected_handle)
		self.calendar.handler_unblock (self.day_selected_double_handle)
		self.calendar_find_date.handler_unblock (self.clicked_handle)
		self.entry_find_date.handler_unblock (self.activate)
		self.entry_find_date.handler_unblock (self.focus_out)
	def search_button_cliked(self,button):
		self.listaSearch = forecasts.searchTodosList(self,self.entry_find_date.get_text ())
		if len(self.listaSearch)==0:
			self.show_message_date_dialog()
		else:
			self.show_list(self.listaSearch)
	def chart_button_cliked(self,button):
		self.listaSearch = forecasts.searchTodosList(self,self.entry_find_date.get_text ())
		if len(self.listaSearch)==0:
			self.show_message_date_dialog()
		else:
			self.plotView=plot.Plot()
			self.show_list(self.listaSearch)
			self.plotView.createPlot(self.listaSearch,self.entry_find_date.get_text ())
	def set_style(self):
		provider = Gtk.CssProvider()
		provider.load_from_path(join(WHERE_AM_I, 'view/gtkdark.css'))
		screen = Gdk.Display.get_default_screen(Gdk.Display.get_default())
		GTK_STYLE_PROVIDER_PRIORITY_APPLICATION = 600
		Gtk.StyleContext.add_provider_for_screen(
			screen, provider,
			GTK_STYLE_PROVIDER_PRIORITY_APPLICATION)
	def show_down_dialog(self):
		self.down_dialog.show()

	def hide_down_dialog(self):
		self.down_dialog.hide()

	def show_message_dialog(self):
		self.message_dialog.run()
		self.message_dialog.hide()
	def show_message_date_dialog(self):
		self.message_date_dialog.run()
		self.message_date_dialog.hide()
	def show_message_dialog_edited(self):
		self.message_edited.run()
		self.message_edited.hide()
	def show_message_dialog_edited_error(self):
		self.message_edited_error.run()
		self.message_edited_error.hide()
	def show_message_no_select_dato(self):
		self.message_no_select_dato.run()
		self.message_no_select_dato.hide()
	def show_message_no_entry_name(self):
		self.message_no_entry_name.run()
		self.message_no_entry_name.hide()

if __name__ == '__main__':
	app = Index()
	Gtk.main()
