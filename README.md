Predicting Climate
==================
This project is a desktop application, developed in Python 3.5. Allows you to see the weather forecast of the next, of a specific city. For which the [pywapi](https://pypi.org/project/pywapi/) library is used, this library obtains the [weather.com](https://weather.com/weather/tenday/l/ECXX0003:1:EC) data. The climate data comes in the form of an array, where index 0 is the current day. For the project, index 1 was used.  

Also have the option where, you can search by date of the consultations made. And you can show a statistical graph for a certain date of the historical data. For graphics the library [matplotlib](https://matplotlib.org/faq/installing_faq.html) is used

No need to install the pywapi library. In the folder logic is the file.

# Functional requirements
In order for the application to run, you need to have it installed:
* [Python 3.5 ](https://www.python.org/downloads/release/python-356/)
* [Mysql Server](https://dev.mysql.com/downloads/mysql/)
* [python3-pymysql](https://pymysql.readthedocs.io/en/latest/)
* [python3-gi](https://pkgs.org/download/python3-gi)
* [python3-matplotlib](https://matplotlib.org/faq/installing_faq.html)
* [python3-cairocffi](https://pypi.org/project/cairocffi/)


# Instructions
* **Installation in ubuntu**.
	* *sudo apt-get install git-core*
	* *sudo apt-get install python3*
	* *sudo apt-get install mysql-server*
	* *sudo apt-get install python3-pymysql*
	* *sudo apt-get install python3-gi*
	* *sudo apt-get install python3-matplotlib*
	* *sudo apt-get install python3-cairocffi*


* **Configuration of the database**.
	* *The user is created: *
		CREATE USER 'weather'@'localhost' IDENTIFIED BY 'root';

	* *The user is given privileges: *
		GRANT ALL PRIVILEGES ON *.* TO 'weather'@'localhost' WITH GRANT OPTION;

	* *The database is created: *
		CREATE DATABASE weatherDB;

	* *The table is created: *
		CREATE TABLE `weatherDB`.forecast_t (
    id int NOT NULL AUTO_INCREMENT,
    name_city varchar(255) NOT NULL,
    forecast_day int,
    forecast_night int,
    creation_time     DATETIME DEFAULT CURRENT_TIMESTAMP,
    modification_time DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id));

* **Run file index.py**.
	* *python3 index.py*

# Testing
This test was conducted on December 14, 2018.

* *The city Guayaquil was entered and they obtained the following results.*

![Alt text](https://bitbucket.org/gustavolonda/predicting-climate/raw/ec66d9de1ba937e5cde3b7ff770eeddb3bf970ed/images/pantalla.png)

* *The cities are entered, Guayaquil, Machala, Pichincha and Salinas. We proceeded to show the statistic chart the weather forecasts.*

![Alt text](https://bitbucket.org/gustavolonda/predicting-climate/raw/ec66d9de1ba937e5cde3b7ff770eeddb3bf970ed/images/chart.png)

* *To edit a data entered in the table, double click on it. The data looks like the data and can be modified.*

![Alt text](https://bitbucket.org/gustavolonda/predicting-climate/raw/ec66d9de1ba937e5cde3b7ff770eeddb3bf970ed/images/edi1.png)

* *Modified data.*

![Alt text](https://bitbucket.org/gustavolonda/predicting-climate/raw/ec66d9de1ba937e5cde3b7ff770eeddb3bf970ed/images/edit2.png)

# Bibliographic references
The bibliographic references can be found in the references.txt file.